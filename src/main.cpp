#include <iostream>

#include "ftxui/dom/elements.hpp"
#include "ftxui/screen/screen.hpp"
#include "ftxui/screen/string.hpp"

int main() {
    namespace ftx = ftxui;

    auto summary = [&] {
        auto content = ftx::vbox({
            ftx::hbox({ftx::text(L"- done:     "), ftx::text(L"3") | ftx::bold}) | ftx::color(ftx::Color::Green),
            ftx::hbox({ftx::text(L"- active:   "), ftx::text(L"2") | ftx::bold}) | ftx::color(ftx::Color::RedLight),
            ftx::hbox({ftx::text(L"- queue:    "), ftx::text(L"9") | ftx::bold}) | ftx::color(ftx::Color::Blue),
        });

        return ftx::window(ftx::text(L" Summary "), content);
    };

    auto document = ftx::vbox({
        ftx::hbox({
            summary(),
            summary(),
            summary() | ftx::flex,
        }),
        summary(),
        summary(),
    });

    // Limit the size of the document to 80 char
    document = document | size(ftx::WIDTH, ftx::LESS_THAN, 80);

    auto screen = ftx::Screen::Create(ftx::Dimension::Full(), ftx::Dimension::Fit(document));
    ftx::Render(screen, document);

    std::cout << screen.ToString() << "\0\n";

    return EXIT_SUCCESS;
}
