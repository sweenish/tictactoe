Tic Tac Toe
A small project to get more familiar with specific tools and design patterns.

## Getting Set Up
- Clone the repository using `git clone --recurse-submodules --remote-submodules`
  - If you did a plain clone already, run `git submodule update --remote` in the root.
- Bootstrap vcpkg
  - In the vcpkg directory, run the appropriate sciprt for your OS.
  - `*.sh` for *nix/macOS, and `*.bat` for Windows.
- In the repository root `cmake -B build -S .`
  - This command will create your build files (makefile, ninja, etc.),
  download the dependencies, and do so "out-of-tree".
  - If you want to play around in the code, copy or move `build/compile_commands.json` to the repository root.
- To build run `cmake --build build` in the repository root.
