#ifndef GRID2D_HPP_
#define GRID2D_HPP_

#include <vector>

template <typename T>
class Grid2D {
public:
    explicit Grid2D(int rows, int columns);

    T* operator[](int idx);

    std::vector<T> data() const;

private:
    std::vector<T> m_grid;
    int m_numRows;
    int m_numColumns;
};

template <typename T>
Grid2D<T>::Grid2D(int rows, int columns) : m_grid(rows * columns, T()), m_numRows(rows), m_numColumns(columns) { }

template <typename T>
T* Grid2D<T>::operator[](int idx) {
    /*
     * [1][1] needs to access index 4; this function just needs to return the proper row
     * 3 * idx + column
     *
     *
     */
    return m_grid.data() + (m_numColumns * idx);
}

template <typename T>
std::vector<T> Grid2D<T>::data() const {
    return m_grid;
}

#endif
