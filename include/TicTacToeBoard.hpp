#ifndef TICTACTOEBOARD_HPP
#define TICTACTOEBOARD_HPP

#include "Grid.hpp"

class TicTacToeBoard {
public:
    TicTacToeBoard() = default;

    void mark(char player, int row, int column);
    bool check_if_won(char player, int row, int column) const;

    std::vector<char> data() const;

private:
    Grid<char> m_grid{3, 3};
};

#endif