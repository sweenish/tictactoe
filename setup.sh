#!/bin/zsh

bootstrap_vcpkg () {
  cd vckpg
  .\bootstrap-vcpkg.sh
  cd ..
}

SKIP_STEP=1

#
# If the vckpkg submodule was not downloaded, download and bootstrap vcpkg
#
if [ -n "$(find . ./vcpkg -maxdepth 0 -empty)" ]; then
  echo "vcpkg submodule not downloaded. Acquiring now."
  git submodule update --init
  bootstrap_vcpkg
  SKIP_STEP=0
fi

#
# Bootstrap vckpg if needed.
#
if [[ "$SKIP_STEP" -eq "1" && ! -f ./vcpkg/vcpkg ]]; then
  echo "vcpkg submodule downloaded but not bootstrapped. Bootstrapping now."
  bootstrap_vcpkg
fi

#
# Create the build directory and copy out the compie commands database
#
cmake -B build -S .
cp build/compile_commands.json ./

